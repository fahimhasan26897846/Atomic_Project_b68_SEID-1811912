<?php

require_once ("../../../vendor/autoload.php");

use App\Message\Message;
use App\Utility\Utility;

use App\BirthDay\BirthDay;

$obj = new BirthDay();
$obj->setData($_GET);

$oneData = $obj->trash();

Utility::redirect("index.php");