<?php

require_once ("../../../vendor/autoload.php");
use App\BirthDay\BirthDay;
use App\Utility\Utility;

$obj = new BirthDay();

$selectedIDs =   $_POST["mark"];

$obj->deleteMultiple($selectedIDs);

$path = $_SERVER['HTTP_REFERER'];

Utility::redirect($path);

