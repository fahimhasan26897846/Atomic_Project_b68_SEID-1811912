﻿<?php
require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION))session_start();

use App\Message\Message;

?>
<!DOCTYPE html>

<html lang="en">

    <head>

        <title>BirthDay Form</title>

        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

        <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap-3.3.7-dist/css/formstyler.css">
        <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap-3.3.7-dist/css/commonmenustylesheet.css">
        <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <style type="text/css">
            .ui-datepicker {
                background: #333;
                border: 1px solid #555;
                color: #EEE;
            }
        </style>



    </head>
    
<body>

        <section class="canvas-wrap">
            <header>
                <div class="commonmenubar">
                    <a href="../../../index.php">Index</a>
                    <a href="index.php">Birthday</a>
                    <a href="../BookTitle/index.php">Book Title</a>
                    <a href="../City/index.php">City</a>
                    <a href="../Hobbies/index.php">Hobbies</a>
                    <a href="../ProfilePicture/index.php">Profile Picture</a>
                    <a href="../SummaryOfOrganization/index.php">Summary of Organization</a>
                    <a href="../Gender/index.php">Gender</a>
                    <a href="../Email/index.php">Email</a>
                </div>
            </header>

            <div class="canvas-content">

              <h1 class="headclass">Birthday Add Form</h1>
                <form action="store.php" method="post">
                    <div class="form-group">
                    <label for="Name">Name:&nbsp;&nbsp;&nbsp;</label>
                    <input type="text" class="form-control" name="Name" placeholder="Name Here" required="">
                    </div>



                    <div class="form-group">
                        <label for="BirthDay">Birth Day:&nbsp;</label>
                        <input type="text"  class="form-control datepicker" name="BirthDay" placeholder="Birthday here"  required="">
                    </div>

                    <button type="submit" class="btn btn-success">Submit</button>
                    <div id="massage"> <?php
                        if(isset($_SESSION['message'])){
                            echo Message::message();
                        }
                        ?>  </div>

                </form>
                </div>

            <div ID="canvas" class="gradient"></div>
        </section>


        <footer>
            <p>Copyright &copy; Atomic Project Powered by BITM PHP B68.</p>
        </footer>


        <!-- Main library -->

        <script src="../../../resources/bootstrap-3.3.7-dist/js/three.min.js"></script>

      
        <!-- Helpers -->
        
        <script src="../../../resources/bootstrap-3.3.7-dist/js/projector.js"></script>

        <script src="../../../resources/bootstrap-3.3.7-dist/js/canvas-renderer.js"></script>


        <!-- Visualitzation adjustments -->

        <script src="../../../resources/bootstrap-3.3.7-dist/js/3d-lines-animation.js"></script>


        <!-- Animated background color -->

        <script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
        <script src="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>

        <script>
            $(function() { $( ".datepicker" ).datepicker({ dateFormat: 'mm-dd-yy' }); });

            $(function ($) {

                    $("#massage").fadeOut(500);
                    $("#massage").fadeIn(500);

                    $("#massage").fadeOut(500);
                    $("#massage").fadeIn(500);

                    $("#massage").fadeOut(500);
                    $("#massage").fadeIn(500);
                    $("#massage").fadeOut(500);

                });



        </script>
        <script src="../../../resources/bootstrap-3.3.7-dist/js/color.js"></script>





</body>
