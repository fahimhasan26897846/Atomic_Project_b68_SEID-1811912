<?php
require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;
use App\BirthDay\BirthDay;

$obj = new BirthDay();
$obj->setData($_GET);
$oneData = $obj->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Edit</title>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap-3.3.7-dist/css/editor%20stylesheet.css">
    <style type="text/css">
        .ui-datepicker {
            background: linear-gradient(#a6e1ec,#5bc0de);
            border: 1px solid #555;
            color: white;
        }
    </style>
    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

</head>
<body>
<header>
    <div class="commonmenubar">
        <a href="../../../index.php">Index</a>
        <a href="index.php">Birthday</a>
        <a href="../BookTitle/index.php">Book Title</a>
        <a href="../City/index.php">City</a>
        <a href="../Hobbies/index.php">Hobbies</a>
        <a href="../ProfilePicture/index.php">Profile Picture</a>
        <a href="../SummaryOfOrganization/index.php">Summary of Organization</a>
        <a href="../Gender/index.php">Gender</a>
        <a href="../Email/index.php">Email</a>
    </div>
</header>

<div class="contentdiv">
    <div class="rectangle"><span>Birth Day Edit Form</span></div>
    <div class="triangle-l"></div>
    <div class="triangle-r"></div>
    <div class="form">

        <form method="post" action="update.php">
            <div class="form-group">
                <label for="User_Name">Name:</label>
                <input type="text" class="form-control"  name="Name" value="<?php echo $oneData->user_name ?>">
            </div>
            <div class="form-group">
                <label for="Birth_Day">Birth Day:</label>
                <input type="text" class="form-control datepicker"  name="BirthDay" value="<?php echo $oneData->date_of_birth ?>">
            </div>
            <div class="form-group">
                <input type="hidden" name="id" value="<?php echo $oneData->id ?>">
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
        <div id="message" class="bg-primary text-center" > <?php if(isset($_SESSION['message'])){
            echo Message::message();
            }
        ?></div>

    </div>

</div>
<footer>
    Copyright &copy; Atomic Project. BITM batch 68.
</footer>
<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
<script src="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>

<script>
    $(function() { $( ".datepicker" ).datepicker({ dateFormat: 'mm-dd-yy' }); });

    $(function ($) {

        $("#massage").fadeOut(500);
        $("#massage").fadeIn(500);

        $("#massage").fadeOut(500);
        $("#massage").fadeIn(500);

        $("#massage").fadeOut(500);
        $("#massage").fadeIn(500);
        $("#massage").fadeOut(500);

    });



</script>
</body>
</html>