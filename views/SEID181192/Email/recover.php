<?php

require_once ("../../../vendor/autoload.php");

use App\Message\Message;
use App\Utility\Utility;

use App\Email\Email;

$obj = new Email();
$obj->setData($_GET);

$oneData = $obj->recover();

Utility::redirect("trashed.php");