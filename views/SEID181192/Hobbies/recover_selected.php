<?php

require_once ("../../../vendor/autoload.php");
use App\Hobbies\Hobbies;
use App\Utility\Utility;

$obj = new Hobbies();

$selectedIDs =   $_POST["mark"];

$obj->recoverMultiple($selectedIDs);


Utility::redirect("trashed.php");

