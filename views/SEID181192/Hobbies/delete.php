<?php

require_once ("../../../vendor/autoload.php");

use App\Message\Message;
use App\Utility\Utility;

use App\Hobbies\Hobbies;

$obj = new Hobbies();
$obj->setData($_GET);

$oneData = $obj->delete();

$path = $_SERVER['HTTP_REFERER'];

Utility::redirect($path);
