<?php

require_once ("../../../vendor/autoload.php");

$obj  = new \App\Hobbies\Hobbies();

$strHobbies = implode(",",$_POST["Hobbies"]);

$_POST["Hobbies"] = $strHobbies;

$obj->setData($_POST);

$obj->store();

\App\Utility\Utility::redirect("index.php");

