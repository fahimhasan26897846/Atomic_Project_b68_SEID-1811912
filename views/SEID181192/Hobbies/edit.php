<?php
require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;
use App\Hobbies\Hobbies;

$obj = new Hobbies();
$obj->setData($_GET);
$oneData = $obj->view();

$hobbiesGet = explode(",",$oneData->hobby);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Edit</title>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap-3.3.7-dist/css/editor%20stylesheet.css">
</head>
<body>
<header>
    <div class="commonmenubar">
        <a href="../../../index.php">Index</a>
        <a href="../BirthDay/index.php">Birthday</a>
        <a href="../BookTitle/index.php">Book Title</a>
        <a href="../City/index.php">City</a>
        <a href="index.php">Hobbies</a>
        <a href="../ProfilePicture/index.php">Profile Picture</a>
        <a href="../SummaryOfOrganization/index.php">Summary of Organization</a>
        <a href="../Gender/index.php">Gender</a>
        <a href="../Email/index.php">Email</a>
    </div>
</header>

<div class="contentdiv">
    <div class="rectangle"><span>Hobby Edit Form</span></div>
    <div class="triangle-l"></div>
    <div class="triangle-r"></div>
    <div class="form">

        <form method="post" action="update.php">
            <div class="form-group">
                <label for="User_Name">Name:</label>
                <input type="text" class="form-control"  name="Name" value="<?php echo $oneData->name?>">
            </div>

            <div class="form-group">
                <label>Hobbies :&nbsp;</label>
                <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox1" name="Hobbies[]" value="Sleeping"
                        <?php
                    if(in_array("Sleeping",$hobbiesGet)) echo "checked"
                    ?>>&nbsp; &nbsp;Sleeping
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox2" name="Hobbies[]"  value="Dancing"
                        <?php
                        if(in_array("Dancing",$hobbiesGet)) echo "checked"
                        ?>>&nbsp;&nbsp; &nbsp;Dancing
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox3" name="Hobbies[]"  value="Gardening"
                            <?php
                        if(in_array("Gardening",$hobbiesGet)) echo "checked"
                        ?>> &nbsp;&nbsp;&nbsp;Gardening
                </label>
            </div>

            <div class="form-group">
                <input type="hidden" name="id" value="<?php echo $oneData->id ?>">
            </div>
            <button  type="submit" class="btn btn-primary">Update</button>
        </form>

    </div>

</div>
<footer>
    Copyright &copy; Atomic Project. BITM batch 68.
</footer>
<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>

<script>


    $(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    });




</script>

</body>
</html>