<?php

require_once ("../../../vendor/autoload.php");

$obj  = new \App\SummaryOfOrganization\SummaryOfOrganization();

$obj->setData($_POST);

$obj->store();

\App\Utility\Utility::redirect("index.php");
