<?php

require_once ("../../../vendor/autoload.php");

use App\Message\Message;
use App\Utility\Utility;

use App\SummaryOfOrganization\SummaryOfOrganization;

$obj = new SummaryOfOrganization();
$obj->setData($_GET);

$oneData = $obj->recover();

Utility::redirect("trashed.php");