<?php

require_once ("../../../vendor/autoload.php");
use App\SummaryOfOrganization\SummaryOfOrganization;
use App\Utility\Utility;

$obj = new SummaryOfOrganization();

$selectedIDs =   $_POST["mark"];

$obj->recoverMultiple($selectedIDs);


Utility::redirect("trashed.php");

