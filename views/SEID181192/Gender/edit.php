<?php
require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;
use App\Gender\Gender;

$obj = new Gender();
$obj->setData($_GET);
$oneData = $obj->view();

$genderGet = explode(",",$oneData->gender);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Edit</title>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap-3.3.7-dist/css/editor%20stylesheet.css">
</head>
<body>
<header>
    <div class="commonmenubar">
        <a href="../../../index.php">Index</a>
        <a href="../BirthDay/index.php">Birthday</a>
        <a href="../BookTitle/index.php">Book Title</a>
        <a href="../City/index.php">City</a>
        <a href="../Hobbies/index.php">Hobbies</a>
        <a href="../ProfilePicture/index.php">Profile Picture</a>
        <a href="../SummaryOfOrganization/index.php">Summary of Organization</a>
        <a href="index.php">Gender</a>
        <a href="../Email/index.php">Email</a>
    </div>
</header>

<div class="contentdiv">
    <div class="rectangle"><span>Gender Edit Form</span></div>
    <div class="triangle-l"></div>
    <div class="triangle-r"></div>
    <div class="form">

        <form method="post" action="update.php">
            <div class="form-group">
                <label for="User_Name">Name:</label>
                <input type="text" class="form-control"  name="Name" value="<?php echo $oneData->name?>">
            </div>

            <div class="form-group ">
                <label for="Gender">Gender</label><br>
                <level>Male<input type="radio" name="Gender" value="male"  <?php
                    if(in_array("male",$genderGet)) echo "checked";
                    ?>>
                </level><br>
                <level>Female<input type="radio" name="Gender" value="female"<?php
                    if(in_array("female",$genderGet)) echo "checked";
                    ?>></level><br>
                <level>Other<input type="radio" name="Gender" value="other"
                        <?php
                        if(in_array("other",$genderGet)) echo "checked";
                        ?>
                    ></level><br>
            </div>

            <div class="form-group">
                <input type="hidden" name="id" value="<?php echo $oneData->id ?>">
            </div>
            <button class="btn btn-primary">Update</button>
        </form>

    </div>

</div>
<footer>
    Copyright &copy; Atomic Project. BITM batch 68.
</footer>
<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>

<script>


    $(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    });




</script>

</body>
</html>