<?php

require_once ("../../../vendor/autoload.php");

$obj  = new \App\Gender\Gender();

$obj->setData($_POST);

$obj->store();

\App\Utility\Utility::redirect("index.php");