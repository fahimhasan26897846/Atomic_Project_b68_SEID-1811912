<?php

require_once ("../../../vendor/autoload.php");
use App\Gender\Gender;
use App\Utility\Utility;

$obj = new Gender();

$selectedIDs =   $_POST["mark"];

$obj->deleteMultiple($selectedIDs);

$path = $_SERVER['HTTP_REFERER'];

Utility::redirect($path);

