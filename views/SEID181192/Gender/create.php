<?php
require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION))session_start();
use App\Message\Message;
?>
<!DOCTYPE html>

<html lang="en">

<head>

    <title>BirthDay Form</title>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap-3.3.7-dist/css/formstyler.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap-3.3.7-dist/css/commonmenustylesheet.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
</head>

<body>

<section class="canvas-wrap">
    <header>

        <div class="commonmenubar">
            <a href="../../../index.php">Index</a>
            <a href="../BirthDay/index.php">Birthday</a>
            <a href="../BookTitle/index.php">Book Title</a>
            <a href="../City/index.php">City</a>
            <a href="../Hobbies/index.php">Hobbies</a>
            <a href="../ProfilePicture/index.php">Profile Picture</a>
            <a href="../SummaryOfOrganization/index.php">Summary of Organization</a>
            <a href="index.php">Gender</a>
            <a href="../Email/index.php">Email</a>
        </div>
    </header>

    <div class="canvas-content">

        <h1 class="headclass">Gender Will </h1>
        <form action="store.php" method="post" class="form-horizontal">

            <div class="form-group">
                <label for="Name">Name</label>
                <input type="text" class="form-control" name="Name" placeholder="Name">
            </div>



            <div class="form-group">
                <label for="Gender">Gender</label><br>
                <level>Male<input type="radio" name="Gender" value="male" ></level><br>
                <level>Female<input type="radio" name="Gender" value="female"></level><br>
                <level>Other<input type="radio" name="Gender" value="other"></level><br>
            </div>




            <button type="submit" class="btn btn-success">Submit</button>
            <div id="massage"> <?php
                if(isset($_SESSION['message'])){
                    echo Message::message();
                }
                ?>  </div>

        </form>
    </div>

    <div ID="canvas" class="gradient"></div>
</section>


<footer>
    <p>Copyright &copy; Atomic Project Powered by BITM PHP B68.</p>
</footer>


<!-- Main library -->

<script src="../../../resources/bootstrap-3.3.7-dist/js/three.min.js"></script>


<!-- Helpers -->

<script src="../../../resources/bootstrap-3.3.7-dist/js/projector.js"></script>

<script src="../../../resources/bootstrap-3.3.7-dist/js/canvas-renderer.js"></script>


<!-- Visualitzation adjustments -->

<script src="../../../resources/bootstrap-3.3.7-dist/js/3d-lines-animation.js"></script>


<!-- Animated background color -->

<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
<script>
    $(function ($) {

        $("#massage").fadeOut(500);
        $("#massage").fadeIn(500);

        $("#massage").fadeOut(500);
        $("#massage").fadeIn(500);

        $("#massage").fadeOut(500);
        $("#massage").fadeIn(500);
        $("#massage").fadeOut(500);

    });


</script>

<script src="../../../resources/bootstrap-3.3.7-dist/js/color.js"></script>


</body>
