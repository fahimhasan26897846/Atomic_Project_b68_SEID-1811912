<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\BookTitle\BookTitle;

$obj = new BookTitle();
$obj->setData($_GET);

$oneData  =  $obj->view();


?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>




    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/viewsimages.css">
    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body>
<header>

    <div class="commonmenubar">
        <a href="../../../index.php">Index</a>
        <a href="../BirthDay/index.php">Birthday</a>
        <a href="index.php">Book Title</a>
        <a href="../City/index.php">City</a>
        <a href="../Hobbies/index.php">Hobbies</a>
        <a href="../ProfilePicture/index.php">Profile Picture</a>
        <a href="../SummaryOfOrganization/index.php">Summary of Organization</a>
        <a href="../Gender/index.php">Gender</a>
        <a href="../Email/index.php">Email</a>
    </div>
</header>



<div class="container" id='container'>



<?php

    echo "
             <h1> Single Profile Information </h1>
               
             <table class='table table-bordered table-striped'>
             
                    <tr>                   
                        <td>  <b>ID</b>  </td>                
                        <td>  <b>$oneData->id</b>  </td>                
                      
                    </tr>
        
                     <tr>                   
                        <td>  <b>Book Title</b>  </td>                
                        <td>  <b>$oneData->book_title</b>  </td>                
                      
                    </tr>
                         
                     <tr>                   
                        <td>  <b>Author Name</b>  </td>                
                        <td>$oneData->author_name </td>
                      
                    </tr>
                    
                    <tr>                  
                        <td class='text-center' colspan='2'>  <a class='btn bg-primary' href='index.php'> Back to Active List</a> </td>
                    </tr>
                
             
             </table>
             
             
             

         ";


    ?>

</div>

</body>
</html>