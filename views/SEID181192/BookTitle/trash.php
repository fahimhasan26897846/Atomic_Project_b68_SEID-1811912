<?php

require_once ("../../../vendor/autoload.php");

use App\Message\Message;
use App\Utility\Utility;

use App\BookTitle\BookTitle;

$obj = new BookTitle();
$obj->setData($_GET);

$oneData = $obj->trash();

Utility::redirect("index.php");