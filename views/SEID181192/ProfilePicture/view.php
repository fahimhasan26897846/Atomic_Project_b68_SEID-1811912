<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\ProfilePicture\ProfilePicture;

$obj = new ProfilePicture();
$obj->setData($_GET);

$oneData  =  $obj->view();

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>


    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/viewsimages.css">
    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body>
<header>

    <div class="commonmenubar">
        <a href="../../../index.php">Index</a>
        <a href="../BirthDay/index.php">Birthday</a>
        <a href="../BookTitle/index.php">Book Title</a>
        <a href="../City/index.php">City</a>
        <a href="../Hobbies/index.php">Hobbies</a>
        <a href="index.php">Profile Picture</a>
        <a href="../SummaryOfOrganization/index.php">Summary of Organization</a>
        <a href="../Gender/index.php">Gender</a>
        <a href="../Email/index.php">Email</a>
    </div>
</header>



<div class="container" id='container'>


    <?php

         echo "
             <h1> Single Profile Information </h1>
               
             <table class='table table-bordered table-striped demo'>
             
                    <tr>                   
                        <td>  <b>ID</b>  </td>                
                        <td>  <b>$oneData->id</b>  </td>                
                      
                    </tr>
        
                     <tr>                   
                        <td>  <b>Name</b>  </td>                
                        <td>  <b>$oneData->name</b>  </td>                
                      
                    </tr>
                         
                     <tr>                   
                        <td>  <b>Profile Picture</b>  </td>                
                        <td><img width='400px' height='400px' src='Uploads/$oneData->photo' title='$oneData->name'> </td>
                      
                    </tr>
                    
                    <tr>                  
                        <td class='text-center' colspan='2'>  <a class='btn bg-primary' href='index.php'> Back to Active List</a>
                         <a class='btn btn-success' href='Uploads/$oneData->photo' download='$oneData->name'>Download</a></td>
                    </tr>
                
             
             </table>
             
             
             

         ";


?>

</div>
<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery.sliphover.min.js"></script>

<script type="text/javascript">
    $(function(){

        $('#container').sliphover();
    })
</script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-46794744-6', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>