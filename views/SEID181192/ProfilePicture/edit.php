<?php
require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;
use App\ProfilePicture\ProfilePicture;

$obj = new ProfilePicture();
$obj->setData($_GET);
$oneData = $obj->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Edit</title>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap-3.3.7-dist/css/editor%20stylesheet.css">
</head>
<body>
<header>
    <div class="commonmenubar">
        <a href="../../../index.php">Index</a>
        <a href="../BirthDay/index.php">Birthday</a>
        <a href="../BookTitle/index.php">Book Title</a>
        <a href="../City/index.php">City</a>
        <a href="../Hobbies/index.php">Hobbies</a>
        <a href="index.php">Profile Picture</a>
        <a href="../SummaryOfOrganization/index.php">Summary of Organization</a>
        <a href="../Gender/index.php">Gender</a>
        <a href="../Email/index.php">Email</a>
    </div>
</header>
<div class="contentdiv">
    <div class="rectangle"><span>Profile picture Edit Form</span></div>
    <div class="triangle-l"></div>
    <div class="triangle-r"></div>
    <div class="col-lg-6">
    <div class="form">

        <form method="post" action="update.php" enctype="multipart/form-data">
            <div class="form-group">
                <input type="hidden" name="id" value="<?php echo $oneData->id ?>">
            </div>

            <div class="form-group">
                <label for="User_Name">Name:</label>
                <input type="text" class="form-control"  name="Name" value="<?php echo $oneData->name?>">
            </div>



            <div class="form-group">
                <label for="Profile_Picture">Photo: &nbsp;&nbsp;</label>
                <input type="file" class="form-control" name="ProfilePicture" value="<?php echo $oneData->photo?>">
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>

    </div>
</div>
    <div class="col-lg-6" style="margin-top: 50px;border-left:1px solid #9c27b0">
        <img width='150px' height='150px' src='Uploads/<?php echo $oneData->photo ?> ' style="border: 2px solid black; padding: 10px; background-color: white;box-shadow: 10px 10px 2px #000;"></div>
</div>
<footer>
    Copyright &copy; Atomic Project. BITM batch 68.
</footer>
<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>

<script>


    $(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    });




</script>

</body>
</html>