<?php
require_once ("../../../vendor/autoload.php");
use App\Utility\Utility;
$obj  = new \App\ProfilePicture\ProfilePicture();

$obj->setData($_POST);
$oneData = $obj->view();

$fileName = $oneData->photo;

if( !empty($_FILES["ProfilePicture"]["name"]) ){

    // Start of physically moving file to its destination
    $fileName =   time().$_FILES["ProfilePicture"]["name"];

    $source = $_FILES["ProfilePicture"]["tmp_name"];

    $destination = "Uploads/".$fileName;

    move_uploaded_file($source, $destination);

    // End of physically moving file to its destination


}

// Start of the process to store file name to the table
$_POST["ProfilePicture"] = $fileName;

$obj->setData($_POST);

$obj->update();

Utility::redirect("index.php");














