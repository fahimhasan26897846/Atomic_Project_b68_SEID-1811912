<?php
require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;
use App\City\City;

$obj = new City();
$obj->setData($_GET);
$oneData = $obj->view();
$cityGet = $oneData->city;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Edit</title>
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/bootstrap-3.3.7-dist/css/editor%20stylesheet.css">
    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

</head>
<body>
<header>
    <div class="commonmenubar">
        <a href="../../../index.php">Index</a>
        <a href="../../../index.php">Birthday</a>
        <a href="../BookTitle/index.php">Book Title</a>
        <a href="index.php">City</a>
        <a href="../Hobbies/index.php">Hobbies</a>
        <a href="../ProfilePicture/index.php">Profile Picture</a>
        <a href="../SummaryOfOrganization/index.php">Summary of Organization</a>
        <a href="../Gender/index.php">Gender</a>
        <a href="../Email/index.php">Email</a>
    </div>
</header>

<div class="contentdiv">
    <div class="rectangle"><span>City Edit Form</span></div>
    <div class="triangle-l"></div>
    <div class="triangle-r"></div>
    <div class="form">

        <form method="post" action="update.php">
            <div class="form-group form-inline">
                <label for="City">City :&nbsp;</label>
                <select id="City" name="City" class="form-control">
                    <option value="Dhaka"
                    <?php
                    if ($cityGet == "Dhaka") echo "selected"
                    ?>
                    >Dhaka</option>
                    <option value="Comilla"
                          <?php  if ($cityGet == "Comilla") echo "selected"
                    ?>
                    >Comilla</option>
                    <option value="Chittagong"
                        <?php
                        if ($cityGet == "Chittagong") echo "selected"
                        ?>
                    >Chittagong</option>
                    <option value="Noakhali"
                        <?php
                        if ($cityGet == "Noakhali") echo "selected"
                        ?>
                    >Noakhali</option>
                    <option value="Borishal"
                        <?php
                        if ($cityGet == "Borishal") echo "selected"
                        ?>
                    >Borishal</option>
                </select>
            </div>
            <div class="form-group">
                <input type="hidden" name="id" value="<?php echo $oneData->id ?>">
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
        <div id="message" class="bg-primary text-center" > <?php if(isset($_SESSION['message'])){
                echo Message::message();
            }
            ?></div>

    </div>

</div>
<footer>
    Copyright &copy; Atomic Project. BITM batch 68.
</footer>
<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
<script>
    $(function ($) {

        $("#massage").fadeOut(500);
        $("#massage").fadeIn(500);

        $("#massage").fadeOut(500);
        $("#massage").fadeIn(500);

        $("#massage").fadeOut(500);
        $("#massage").fadeIn(500);
        $("#massage").fadeOut(500);

    });



</script>
</body>
</html>