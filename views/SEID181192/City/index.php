<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Utility\Utility;
use App\Message\Message;
use App\City\City;


$obj = new City();

$allData  =  $obj->index();
################## search  block 1 of 5 start ##################
if(isset($_REQUEST['search']) )$someData =  $obj->search($_REQUEST);
$availableKeywords=$obj->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';

################## search  block 1 of 5 end ##################


######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $obj->indexPaginator($page,$itemsPerPage);
$allData= $someData;
$serial = (($page-1) * $itemsPerPage) +1;

####################### pagination code block#1 of 2 end #########################################




################## search  block 2 of 5 start ##################

if(isset($_REQUEST['search']) ) {
    $someData = $obj->search($_REQUEST);
    $allData = $someData;
    $serial = 1;
}
################## search  block 2 of 5 end ##################



?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>


    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.min.css">
    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bintrstyle.css">
    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>




</head>
<body>
<header>
    <div class="commonmenubar">
        <a href="../../../index.php">Index</a>
        <a href="../BirthDay/index.php">Birthday</a>
        <a href="../BookTitle/index.php">Book Title</a>
        <a href="../City/index.php">City</a>
        <a href="../Hobbies/index.php">Hobbies</a>
        <a href="../ProfilePicture/index.php">Profile Picture</a>
        <a href="../SummaryOfOrganization/index.php">Summary of Organization</a>
        <a href="../Gender/index.php">Gender</a>
        <a href="../Email/index.php">Email</a>
    </div>
</header>
<div class="container">
<div style="height: 20px">
    <div id="massage" style=" text-align: center; background: linear-gradient(#5bc0de,#2e6da4);color: white"> <?php
        if(isset($_SESSION['message'])){
            echo Message::message();
        }
        ?>  </div>
</div>





<div>

    <div class="nav navbar-default btn-group">
        <a href='create.php' class='btn  btn-success'>Create</a>
        <a href='trashed.php' class='btn  btn-info'>Trashed List</a>
        <button id="TrashSelected" class='btn  btn-primary'>Trash Selected</button>

        <button id="DeleteSelected" class='btn  btn-danger confirm'>Delete Selected</button>
        <a href='pdf.php' class='btn  btn-warning'>Download As PDF</a>


        <a href='xl.php' class='btn  btn-info'>Download As XL</a>


        <a href='email.php' class='btn  btn-danger'>Email This List</a>


    </div>

    <div class="bg-info text-center"><h1>City Active List - Active List(<?php echo count($obj->index());?>)</h1></div>

    <table border="1px" class="table table-bordered table-striped">

        <tr>
            <th> Check All &nbsp;<input type="checkbox" id="select_all" value="check all"></th>
            <th> Serial </th>
            <th> ID </th>
            <th> City </th>
            <th> Action Buttons </th>

        </tr>
        <!-- required for search, block 4 of 5 start -->

        <div style="margin-left: 70%;margin-bottom: 10px">
            <form id="searchForm" action="index.php"  method="get" style="margin-top: 45px;">
                <input type="text" value="" id="searchID" name="search" placeholder="Search" width="60" style="border-radius: 5%" >
                <input type="checkbox"  name="byCity"  checked >By City
                <input hidden type="submit" class="btn-primary" value="search">
            </form>
        </div>

        <!-- required for search, block 4 of 5 end -->

<form id="multiple" method="post">
        <?php


        //$serial=1;

        foreach ($allData as $oneData){

            if($serial%2) $bgColor = "#a6e1ec";
            else $bgColor = "#ffffff";

            echo "
    
                                  <tr  style='background-color: $bgColor'>
    
                                     <td style='padding-left: 4%'><input type='checkbox' class='checkbox' name='mark[]' value='$oneData->id'></td>
    
                                     <td style='width: 10%; text-align: center'>$serial</td>
                                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                                     <td>$oneData->city  </td>
    
                                     <td>
                                       <a href='view.php?id=$oneData->id' class='btn btn-info'>View</a>
                                       <a href='edit.php?id=$oneData->id' class='btn btn-primary'>Edit</a>
                                       <a href='trash.php?id=$oneData->id' class='btn btn-warning'>Trash</a>
                                       <a href='delete.php?id=$oneData->id' class='btn btn-danger confirm'>Delete</a>
                                       <a href='email.php?id=$oneData->id' class='btn btn-success'>Email</a>
    
                                     </td>
                                  </tr>
                              ";
            $serial++;

        }

        ?>
</form>
    </table>

    <!--  ######################## pagination code block#2 of 2 start ###################################### -->
    <div align="right">
        <ul class="pagination">

            <?php

            $pageMinusOne  = $page-1;
            $pagePlusOne  = $page+1;

            if($page>$pages) Utility::redirect("index.php?Page=$pages");

            if($page>1)  echo "<li><a href='index.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";
            for($i=1;$i<=$pages;$i++)
            {
                if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
                else  echo "<li><a href='index.php?Page=$i'>". $i . '</a></li>';

            }
            if($page<$pages) echo "<li><a href='index.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

            ?>

            <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
                <?php
                if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

                if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
                else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

                if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

                if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

                if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

                if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
                else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
                ?>
            </select>
        </ul>
    </div>
    <!--  ######################## pagination code block#2 of 2 end ###################################### -->


</div>

</div>
<footer>
    <p><b>Copyright &copy; Atomic project . Powered by BITM batch 68</b></p>
</footer>
<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
<script src="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
<script src="../../../resources/jquery-ui-1.12.1.custom/dialogue.js"></script>

<script>
    $(document).ready(function () {

        $("#TrashSelected").click(function () {

            $("#multiple").attr("action","trash_selected.php");
            $("#multiple").submit();
        });

        $("#DeleteSelected").click(function () {

            $("#multiple").attr("action","delete_selected.php");
            $("#multiple").submit();
        });


    });




    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){ //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

//check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });


    $(".confirm").easyconfirm({locale: { title: 'Are you sure to delete the element?', button: ['No','Yes']}});

    $(function ($) {

        $("#massage").fadeOut(500);
        $("#massage").fadeIn(500);

        $("#massage").fadeOut(500);
        $("#massage").fadeIn(500);

        $("#massage").fadeOut(500);
        $("#massage").fadeIn(500);
        $("#massage").fadeOut(500);

    });


</script>
<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block5 of 5 end -->


</body>
</html>